import { Button } from '@material-ui/core';
import React from 'react';
import {useHistory} from "react-router-dom";

const Error =()=>{
    const history = useHistory()
    const fn = ()=>{
        history.push("/profile");
    }
    return (
        <>
        <h1>404:Page Not Found</h1>
        <Button fullWidth variant="contained" color="primary" onClick={fn}>Back to site</Button>
        </>
    )
}
export default Error;