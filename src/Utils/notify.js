import { toast } from 'react-toastify';

function showSuccess(msg) {
    toast.success(msg);
}
function showInfo(msg) {
    toast.info(msg);
}
function showWarning(msg) {
    toast.warn(msg);
}

// prepare a generc function to handle Errors
function handleError(error) {
    debugger;
    let defaultErrMsg = 'something went wrong';
    if (error && error.response) {
        defaultErrMsg = error.response.data.message;
    }
    // check what comes in
    // parse incoming error message
    // show then in ui
    _showError(defaultErrMsg);
}

function _showError(msg) {
    toast.error(msg);
}

export default {
    showSuccess,
    showInfo,
    showWarning,
    handleError
}
