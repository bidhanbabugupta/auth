import React, { useState } from 'react';
import {NavLink} from "react-router-dom"
import axios from "axios";
import Snack from "./../Snack/snack";
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import notify from './../../Utils/notify';

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(3),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

function Register() {
    const classes = useStyles();
    const [data,SetData] = useState({userName:'',password:'',email:'',name:''});
    const [err,SetErr] = useState({userNameErr:'',passwordErr:'',emailErr:'',nameErr:''});
    const [open,SetOpen] = useState(false);
    const [message,SetMessage] = useState("");
    const handleClick = () => {
        SetOpen(false);
      };
    const inputEvent=(event)=>{
        
        const {name,value} = event.target;
        SetData((preValue)=>{
            return{
                ...preValue,
                [name]: value 
            }
        })
    }

    const onSubmits=(event)=>{
            event.preventDefault();
            if(data.userName===""){
                SetErr((preValue)=>{
                    return{
                        ...preValue,
                        userNameErr: "Please enter username"
                    }
                })
            }else if(data.password.length<6){
                SetErr((preValue)=>{
                    return{
                        ...preValue,
                        passwordErr: "Minimum Length is 6"
                    }
                })
            }else if(data.name===""){
                SetErr((preValue)=>{
                    return{
                        ...preValue,
                        nameErr: "Please enter name"
                    }})
            }
            else{
            axios.post('http://localhost:3001/auth/register', data)
              .then(function (response) {
                console.log(response);
                SetData({userName:'',password:'',email:'',name:''});
                SetErr({userNameErr:'',passwordErr:'',emailErr:'Check Your email to activate your account',nameErr:''})
                SetMessage("Check Your email to activate your account");
                notify.showSuccess("Check Your email to activate your account")
                SetOpen(true);
              })
              .catch(function (error) {
                console.log(error);
                SetErr({userNameErr:'',passwordErr:'',emailErr:error.response.data.message,nameErr:''})
                notify.handleError(error)
              });
        }
    }
    
    return (
      <>
      <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <Snack open={open} message={message} hclick={handleClick}/> 
        <small style={{color:"green"}}>{err.emailErr}</small><br/>
            <form className={classes.form} noValidate onSubmit={onSubmits}>
                <TextField type="text" label="Enter your name" fullWidth name="name" autoFocus onChange={inputEvent} value={data.name} required/><br/>
                <small style={{color:"red"}}>{err.nameErr}</small><br/>
                <TextField  type="text" label="Enter your Username" fullWidth name="userName" onChange={inputEvent}  value={data.userName} required /><br/>
                <small style={{color:"red"}}>{err.userNameErr}</small><br/>
                <TextField  type="password" label="Enter your password" fullWidth name="password" onChange={inputEvent} value={data.password} required/><br/>
                <small style={{color:"red"}}>{err.passwordErr}</small><br/>
                <TextField  type="email" label="Enter your email" fullWidth name="email" onChange={inputEvent} value={data.email} required/><br/>
                <Button fullWidth variant="contained" color="primary" className={classes.submit} type="submit">Sign Up</Button>
                <Grid container justify="flex-end">
                <Grid item>
                <NavLink variant="body2" exact to="/">Back to login</NavLink><br/><br/>
                </Grid>
                </Grid>
            </form><br/>
        </div>
        </Container>
      </>
    );
  }
  
  export default Register;