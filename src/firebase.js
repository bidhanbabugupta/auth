// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import firebase from 'firebase';
const firebaseConfig = {
    apiKey: "AIzaSyAokvCVdnDyQ4U3TVYvAPKcfmFTvriAmbs",
    authDomain: "auth-d7238.firebaseapp.com",
    databaseURL: "https://auth-d7238.firebaseio.com",
    projectId: "auth-d7238",
    storageBucket: "auth-d7238.appspot.com",
    messagingSenderId: "88080126192",
    appId: "1:88080126192:web:13628d220161d34305b235",
    measurementId: "G-NRJBFCDMZ5"
  };
  const firebaseApp = firebase.initializeApp(firebaseConfig);
  const auth = firebase.auth();
  const provider = new firebase.auth.GoogleAuthProvider();

  export { auth,provider };