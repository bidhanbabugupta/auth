import React from 'react';
import Login from './Auth/Login/login';
import ForgetPassword from './Auth/Forget-Password/forget-password';
import Register from './Auth/Register/register';
import Reset from './Auth/Reset/reset';
import Profile from './profile/profile';
import { BrowserRouter, Redirect,  Switch } from 'react-router-dom';
import './App.css';
import PrivateRoute from './Utils/PrivateRoute';
import PublicRoute from './Utils/PublicRoute';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';

function App() {
  return (
    <div>
    
    <BrowserRouter>
    <Switch>
    <PublicRoute exact path='/' component={Login}/>
    <PublicRoute  path='/forget' component={ForgetPassword}/>
    <PublicRoute  path='/register' component={Register}/>
    <PublicRoute  path='/auth/reset-password/:id' component={Reset}/>
    <PrivateRoute  path='/profile' component={Profile}/>
    {/* <Route component={Error}/> */}
    <Redirect to="/"/>
    </Switch>
    </BrowserRouter>
    <ToastContainer></ToastContainer>
    </div>
  );
}

export default App;
